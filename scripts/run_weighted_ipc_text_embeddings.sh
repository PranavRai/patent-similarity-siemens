#!/bin/bash
#SBATCH --output=./text_embeddings2.log  # send stdout to outfile

cd ..
source venv/bin/activate
export PYTHONPATH=$PYTHONPATH:./src/
python ./src/runnables/runnable_weighted_ipc_text_embeddings.py
