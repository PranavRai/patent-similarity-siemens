#!/bin/bash
#SBATCH --output=./compare_sim.log  # send stdout to outfile

cd ..
source venv/bin/activate
export PYTHONPATH=$PYTHONPATH:./src/
python ./src/runnables/runnable_compare_similarities.py
