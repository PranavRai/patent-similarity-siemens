#!/bin/bash
#SBATCH --output=./eval_consistency.log  # send stdout to outfile

cd ..
source venv/bin/activate
export PYTHONPATH=$PYTHONPATH:./src/
python ./src/runnables/runnable_eval_consistency.py
