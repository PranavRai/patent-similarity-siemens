#!/bin/bash
#SBATCH --output=./text_embeddings.log  # send stdout to outfile

#rm -rf text_embeddings.log

cd ..
source env/bin/activate
export PYTHONPATH=$PYTHONPATH:./src/
python ./src/features/text_embeddings.py
