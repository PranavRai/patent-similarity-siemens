#!/bin/bash
#SBATCH --output=./finetune_bert.log  # send stdout to outfile

cd ..
source venv/bin/activate
export PYTHONPATH=$PYTHONPATH:./src/
python ./src/runnables/runnable_finetune_bert.py
