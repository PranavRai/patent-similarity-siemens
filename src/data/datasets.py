import pandas_profiling
import pickle
import networkx as nx
import pandas as pd
import logging
from tqdm import tqdm
import numpy as np
from typing import List
from torch.utils.data import Dataset

from src import REPORTS_PATH, DATA_PATH

tqdm.pandas()


class TripletsDataset(Dataset):
    """Dataset for offline triplet mining"""

    def __init__(self, list_of_triplets):
        self.list_of_triplets = list_of_triplets

    def __len__(self):
        return len(self.list_of_triplets)

    def __getitem__(self, idx):
        return self.list_of_triplets[idx]


class PatentsDataset:
    """Class for storing dataset of patents"""
    def __init__(self,
                 path_to_patents: str,
                 nrows: int = None,
                 from_year: int = None,
                 ipc_tags_format: str = 'list',
                 clean_df: bool = False,
                 path_to_citations: str = None,
                 path_to_ipc_tags_embeddings_pkl: str = None,
                 join_citations: bool = False):
        """
        :param path_to_patents: Path with CSV file
        :param nrows: Number of rows to read from CSV (if None - reading whole file)
        :param from_year: Discard patents, older than from_year (this changes cit_numbers_bidir_pres column,
        if join_citations is True)
        :param ipc_tags_format: How IPC tags are stored in csv (list, semicolon)
        :param clean_df: Perform cleaning (look to clean method for details)
        :param path_to_citations: Path to citations.csv
        :param path_to_ipc_tags_embeddings_pkl: Path to embeddings for IPC tags descriptions
        :param join_citations: Join citations numbers to patents dataframe
        """

        logging.info(f'Start reading data.')
        self.patents = pd.read_csv(path_to_patents, nrows=nrows)
        logging.info(f'Read {path_to_patents}. Number of patents: {len(self.patents):,}.')

        if ipc_tags_format == 'list':
            self.patents.ipc = self.patents.ipc.apply(eval)
        elif ipc_tags_format == 'semicolon':
            self.patents.ipc = self.patents.ipc.apply(lambda tag: tag.split(';'))

        if clean_df:
            logging.info(f'Start cleaning.')
            self.clean()
            logging.info(f'Number of patents after cleaning: {len(self.patents):,}.')
        else:
            self.patents.date = pd.to_datetime(self.patents.date)
            logging.info(f'Skip cleaning.')

        if from_year is not None:
            self.patents = self.patents[self.patents.date >= f'{from_year}-01-01']
            logging.info(f'Dropped patents, older than {from_year}-01-01. Number of patents: {len(self.patents):,}.')

        if path_to_citations is not None:
            logging.info(f'Start reading citations.')
            self.citations = pd.read_csv(path_to_citations)
            logging.info(f'Read {path_to_citations}. Number of citations: {len(self.citations):,}.')

            if join_citations:

                logging.info(f'Joining bidirectional citations to self.patents (full lists).')
                reversed_citations = self.citations.rename(columns={'pub_number': 'cit_number', 'cit_number': 'pub_number'})
                bidir_citations = pd.concat([reversed_citations, self.citations], sort=True)
                cited_patents = self.patents.merge(bidir_citations, on='pub_number'
                                                   ).groupby('pub_number')['cit_number'].apply(list)
                self.patents = self.patents.join(cited_patents, on='pub_number')
                self.patents.rename(columns={'cit_number': 'cit_numbers_bidir_full'}, inplace=True)

                logging.info(f'Joining bidirectional citations to self.patents (lists of only present citations).')
                bidir_citations = bidir_citations[bidir_citations.cit_number.isin(self.patents.pub_number) &
                                                  bidir_citations.pub_number.isin(self.patents.pub_number)]
                cited_patents = self.patents.merge(bidir_citations, on='pub_number'
                                                   ).groupby('pub_number')['cit_number'].apply(list)
                self.patents = self.patents.join(cited_patents, on='pub_number')
                self.patents.rename(columns={'cit_number': 'cit_numbers_bidir_pres'}, inplace=True)

            logging.info(f'Start creating undirected citations graph.')
            self.citations_graph = nx.from_pandas_edgelist(self.citations, source='pub_number', target='cit_number')
            logging.info(f'Citations graph created. Number of edges: {len(self.citations_graph.edges):,}. '
                         f'Number of nodes: {len(self.citations_graph.nodes):,}')

        # if path_to_doc2vec_embeddings is not None:
        #     logging.info(f'Reading doc2vec embeddings.')
        #     doc2vec_embeddings = np.load(path_to_doc2vec_embeddings)['embeddings']
        #     assert len(doc2vec_embeddings) == len(self.patents)
        #     self.patents['absract_doc2vec'] = list(doc2vec_embeddings)
        #
        # if path_to_bert_embeddings is not None:
        #     logging.info(f'Reading bert embeddings.')
        #     bert_embeddings = np.load(path_to_bert_embeddings)['embeddings']
        #     assert len(bert_embeddings) == len(self.patents)
        #     self.patents['absract_bert'] = list(bert_embeddings)

        if path_to_ipc_tags_embeddings_pkl is not None:
            logging.info(f'Reading ipc tags embeddings.')
            with open(path_to_ipc_tags_embeddings_pkl, "rb") as r:
                self.ipc_embeddings_map = pickle.load(r)

    def clean(self, reset_index=True):
        """
        Cleaning raw patents df: unifying date, sorting by date, dropping NANs, dropping 'Published without abstract.',
        dropping duplicating ipc tags
        :param reset_index: Resetting index
        :return:
        """
        # Simplifying date = pub_date (if pub_date==0, then use filing_date or priority_date)
        if 'pub_date' in self.patents.columns:
            self.patents['date'] = self.patents.pub_date
            self.patents.loc[self.patents.date == 0, 'date'] = self.patents[self.patents.date == 0].filing_date
            self.patents.loc[self.patents.date == 0, 'date'] = self.patents[self.patents.date == 0].priority_date
            self.patents.date = pd.to_datetime(self.patents.date.astype(str), format='%Y%m%d')
            self.patents.drop(['pub_date', 'filing_date', 'priority_date'], axis=1, inplace=True)

        # Sorting descending by date - important for droping duplicates
        self.patents.sort_values(by='date', ascending=False, inplace=True)

        # Dropping patents without title or abstract
        self.patents.dropna(subset=['title', 'abstract'], inplace=True)
        self.patents = self.patents[~(self.patents.abstract == 'Published without abstract.')]

        # Dropping patents with duplicating pub_number
        self.patents.drop_duplicates(subset=['pub_number'], inplace=True)
        # Dropping patents with duplicating abstract
        # self.patents.drop_duplicates(subset=['abstract'], inplace=True)

        if reset_index:
            self.patents.reset_index(inplace=True, drop=True)

        # Dropping duplicating ipc tags (in scope of every patent)
        self.patents.ipc = self.patents.ipc.apply(lambda ipc_list: list(dict.fromkeys(ipc_list)))

    def generate_patents_eda(self, name='eda_clean.html'):
        """
        Generate EDA with pandas_profilling
        :param name: Filename in REPORTS_PATH
        """
        profile = self.patents.profile_report()
        profile.to_file(output_file=f'{REPORTS_PATH}/{name}')


class PatentsPairsDataFrame:
    """
    Patent pairs, used to evaluate similarities. Columns with suffix 'x' correspond to queries (anchors) properties,
    'y' - positives and negatives
    """
    def __init__(self, patents: PatentsDataset = None, name: str = "", from_csv: str = None,
                 n_samples=500, n_pos_neg_per_sample=500, random_seed=42,
                 queries_to_exclude: List = None, from_year=2000, till_year=2020,
                 features_columns=('pub_number', 'ipc', 'date', 'abstract', 'title')):
        """
        :param patents: PatentsDataset to sample from
        :param name: train/test/val (import for saving/visualizing)
        :param from_csv: Path to existing csv
        :param n_samples: Number of patents (for each of them positives/negatives will be selected) to select
        (if None - all are used)
        :param n_pos_neg_per_sample: Total number of positive and negative samples for each patent
        :param random_seed: Random seed
        :param queries_to_exclude: Queries to exclude, for random-based split
        :param from_year: Sample queries from from_year
        :param till_year: Sample queries till till_year
        :param features_columns: List of columns to save in pairs_df
        """
        if from_csv is None:
            logging.info(f'Start creating evaluation dataframe of patent pairs.')

            if patents is None:
                logging.error('Need to provide PatentsDataset!')

            # Selecting edges, present in PatentsDataset
            present_citations = patents.citations[patents.citations.pub_number.isin(patents.patents.pub_number) &
                                                  patents.citations.cit_number.isin(patents.patents.pub_number)]

            # Intermediate DataFrames for storing
            query_patents = pd.merge(present_citations, patents.patents, left_on='pub_number', right_on='pub_number')
            if queries_to_exclude is not None:
                query_patents = query_patents[~query_patents.pub_number.isin(queries_to_exclude)]
            query_patents = query_patents[(query_patents.date >= f'{from_year}-01-01') &
                                          (query_patents.date < f'{till_year}-01-01')]
            query_patents_pub_numbers = query_patents.pub_number.drop_duplicates()

            # Sampling rows of similarity matrix
            if n_samples is not None:
                assert len(query_patents_pub_numbers) >= n_samples
                query_patents_pub_numbers = query_patents_pub_numbers.sample(n_samples, random_state=random_seed)
                query_patents = query_patents[query_patents.pub_number.isin(query_patents_pub_numbers)]

            query_patents_unique = query_patents.drop(columns='cit_number').drop_duplicates(subset='pub_number')
            query_patents_unique = query_patents_unique.join(query_patents.groupby('pub_number')['cit_number'].apply(list),
                                                             on='pub_number')

            # Sampling columns of similarity matrix
            features_columns = list(features_columns)
            logging.info(f'Sampling columns of similarity matrix. Creating positive pairs.')
            positive_pairs_df = pd.merge(query_patents[features_columns + ['cit_number']],
                                         patents.patents[features_columns], left_on='cit_number', right_on='pub_number')
            positive_pairs_df = positive_pairs_df.drop(columns='cit_number')

            logging.info(f'Sampling columns of similarity matrix. Creating negative pairs.')
            negative_patents_dfs = []
            for index, row in tqdm(query_patents_unique.iterrows(), total=len(query_patents_unique)):
                n_negatives = n_pos_neg_per_sample - sum(positive_pairs_df.pub_number_x == row['pub_number'])
                assert n_negatives >= 0
                older_negative_patents = patents.patents[(patents.patents.date < row['date']) &
                                                         ~(patents.patents.pub_number.isin(row['cit_number']))]
                assert len(older_negative_patents) >= n_negatives
                older_negative_patents_sample = older_negative_patents.sample(n_negatives, random_state=index)
                older_negative_patents_sample['negatives_to'] = row['pub_number']

                negative_patents_dfs.append(older_negative_patents_sample)
            negative_pairs_df = pd.merge(query_patents_unique[features_columns],
                                         pd.concat(negative_patents_dfs)[features_columns + ['negatives_to']],
                                         left_on='pub_number', right_on='negatives_to')
            negative_pairs_df = negative_pairs_df.drop(columns='negatives_to')

            self.pairs_df = pd.concat([positive_pairs_df, negative_pairs_df])
            self.query_patents_list = query_patents.pub_number.unique()
            logging.info(f'Evaluation data-frame created.')

        else:
            logging.info(f'Reading existing pairs_df from {from_csv}.')
            self.pairs_df = pd.read_csv(from_csv)
            self.query_patents_list = self.pairs_df.pub_number_x.unique()

            if 'ipc' in features_columns:
                self.pairs_df.ipc_x = self.pairs_df.ipc_x.apply(eval)
                self.pairs_df.ipc_y = self.pairs_df.ipc_y.apply(eval)

            if 'date' in features_columns:
                self.pairs_df.date_x = pd.to_datetime(self.pairs_df.date_x)
                self.pairs_df.date_y = pd.to_datetime(self.pairs_df.date_y)

            logging.info(f'Read pairs_df. Number of pairs: {len(self.pairs_df)}.')

        if patents is not None:
            self.citations_graph = patents.citations_graph
            self.ipc_embeddings_map = patents.ipc_embeddings_map

            logging.info(f'Calculating mean ipc textual embeddings.')
            self.pairs_df['ipc_bert_x'] = self.pairs_df.ipc_x.apply(self._mean_ipc_embedding)
            self.pairs_df['ipc_bert_y'] = self.pairs_df.ipc_y.apply(self._mean_ipc_embedding)

        self.similarity_columns = set()
        self.name = name
        self.dim = (len(self.query_patents_list), len(self.pairs_df) // len(self.query_patents_list))

    def save_pairs_df(self, columns_to_drop=[]):
        """
        Saving pairs_df as csv
        """
        path = f'{DATA_PATH}/{self.name}_pairs_{self.dim[0]}*{self.dim[1]}.csv'
        logging.info(f'Saving pairs_df to {path}.')
        self.pairs_df.drop(columns=columns_to_drop).to_csv(path, index=False)

    def number_of_possible_triplets(self):
        """
        Calculate overall nuber of possible triplets
        """
        grouped = self.pairs_df.fillna(0.0).groupby(['pub_number_x', 'x_citation_1_hop_sim']).size().unstack()
        return sum(grouped[0.0] * grouped[1.0])

    def _mean_ipc_embedding(self, ipc_tags_list):
        return np.array([emb for emb in map(self.ipc_embeddings_map.get, ipc_tags_list) if emb is not None]).mean(axis=0)

    def visualise_pairs_df(self, ax, time_limits=(pd.to_datetime('2020-01-01'), pd.to_datetime('1999-01-01'))):
        """
        Visualizing pairs_df on a time axis, as a rug plot
        :param ax: Axis
        :param time_limits: Limits for time axis on plot
        """
        pub_numbers = self.query_patents_list

        from src.models.similarities import BidirXCitationSimilarity
        x_cit_sim = BidirXCitationSimilarity(citations_graph=self.citations_graph, n_hops=1)
        x_cit_sim.evaluate(self)

        for i, pub_number in enumerate(pub_numbers):
            x_neg = self.pairs_df[(self.pairs_df.pub_number_x == pub_number) &
                                  self.pairs_df.x_citation_1_hop_sim.isna()].date_y
            x_pos = self.pairs_df[(self.pairs_df.pub_number_x == pub_number) &
                                  (self.pairs_df.x_citation_1_hop_sim == 1.0)].date_y
            x_self = self.pairs_df[self.pairs_df.pub_number_x == pub_number].date_x.iloc[0]

            ax.plot(x_neg, np.zeros(len(x_neg)) + i / 2, '|', ms=10, color='grey', markeredgewidth=1,
                    label='Negatives' if i == 0 else None)
            ax.plot(x_pos, np.zeros(len(x_pos)) + i / 2, '|', ms=12, color='red', markeredgewidth=2,
                    label='Positives' if i == 0 else None)
            ax.plot(x_self, i / 2, '|', ms=12, markeredgewidth=3, color='royalblue',
                    label='Query patent' if i == 0 else None)

        ax.set_xlim(*time_limits)
        ax.set_yticks(np.arange(0, len(pub_numbers) // 2, 0.5))
        ax.set_yticklabels(pub_numbers)
        ax.grid()
        ax.set_title(self.name)
        ax.set_xlabel('Publishing time')
        ax.set_ylabel('Query patents')
        ax.legend()

    def to_triplets_dataset(self, txt_feature='abstract') -> TripletsDataset:
        cols_subset = ['pub_number_x', 'pub_number_y', f'{txt_feature}_y']
        tripples_df = pd.merge(self.pairs_df[self.pairs_df.x_citation_1_hop_sim == 1.0][cols_subset + [f'{txt_feature}_x']],
                               self.pairs_df[self.pairs_df.x_citation_1_hop_sim != 1.0][cols_subset],
                               how='outer',
                               on='pub_number_x',
                               suffixes=('_pos', '_neg'))
        return TripletsDataset([tuple(row) for row in
                                tripples_df[[f'{txt_feature}_x', f'{txt_feature}_y_pos', f'{txt_feature}_y_neg']].values])
