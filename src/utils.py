from google_drive_downloader import GoogleDriveDownloader as gdd
import gzip
import shutil
import os
import hashlib
import re
from typing import List


def split_ipc_levels(ipc: str) -> List[str]:
    """
    Splitting IPC tag string to list with 5 levels
    :param ipc: IPC tag string
    :return: list with 5 levels
    """
    result = re.search('([A-Z])([0-9]+)([A-Z]) *([0-9]*)/([0-9]*)', ipc)
    return result.groups()


def download_from_google_drive(path, name):
    """Downloading data from Google drive"""
    print(f'Enter google-drive file id ({name}):')
    file_id = input()
    gdd.download_file_from_google_drive(file_id=file_id, dest_path=f'{path}.gz',
                                        unzip=False, showsize=True, overwrite=True)
    with gzip.open(f'{path}.gz', 'rb') as f_in:
        with open(path, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

    os.remove(f'{path}.gz')


def md5_hash(pub_numbers: List[str]) -> str:
    """Calculating md5 hash for list of pub_numbers (internally sorts list)"""
    result = hashlib.md5(' '.join(sorted(pub_numbers)).encode('utf-8'))
    return result.hexdigest()
