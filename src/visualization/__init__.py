import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


def visualise_mrr_matrix(mrr_matrix: pd.DataFrame):
    fig, ax = plt.subplots(figsize=(len(mrr_matrix), len(mrr_matrix)))
    sns.heatmap(mrr_matrix, annot=True, vmin=0.0, vmax=1.0, fmt='.3f', square=True, ax=ax, cmap='Greens')
    ax.set_title('Mean reciprocal rank matrix')
    ax.set_ylabel('ground-truth similarities')
    ax.set_xlabel('proposed similarities')
    plt.show()
