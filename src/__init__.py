from os.path import abspath, dirname, exists
from os import mkdir
import logging
from src.utils import download_from_google_drive
import pandas as pd
import nltk

logging.basicConfig(level=logging.INFO)
pd.options.mode.chained_assignment = None

ROOT_PATH = dirname(dirname(abspath(__file__)))
IPC_URL = "https://www.wipo.int/ipc/itos4ipc/ITSupport_and_download_area/" \
          "20190101/IPC_scheme_title_list/EN_ipc_section_A_title_list_20190101.txt"

DATA_PATH = f'{ROOT_PATH}/data'
RESOURCES_PATH = f'{ROOT_PATH}/resources'
mkdir(DATA_PATH) if not exists(DATA_PATH) else None
mkdir(RESOURCES_PATH) if not exists(RESOURCES_PATH) else None

# Data
PATENTS_PATH = f'{DATA_PATH}/patent-contents-for-citations-only-type-x.csv'
CLEAN_PATENTS_PATH = f'{DATA_PATH}/patents-clean.csv'
CLEAN_100K_PATENTS_PATH = f'{DATA_PATH}/patents-clean-from-2000-100k.csv'
CITATIONS_PATH = f'{DATA_PATH}/citations-only-type-x.csv'

# Resources
TAGS_PATH = f'{RESOURCES_PATH}/tags'
WORD2VEC_MODEL_GOOGLE_PATH = f'{RESOURCES_PATH}/GoogleNews-vectors-negative300.bin'
TEXT_EMBEDDING_PATH = f'{RESOURCES_PATH}/embeddings'
DOC2VEC_MODEL_PATH = f'{RESOURCES_PATH}/doc2vec'
BERT_MODEL_PATH = f'{RESOURCES_PATH}/bert'
IPC_TAGS_PATH = f'{RESOURCES_PATH}/ipc_tags.csv'
IPC_TAGS_100k_PATH = f'{RESOURCES_PATH}/ipc_tags-from-2000-100k.csv'

mkdir(TAGS_PATH) if not exists(TAGS_PATH) else None
mkdir(TEXT_EMBEDDING_PATH) if not exists(TEXT_EMBEDDING_PATH) else None
mkdir(DOC2VEC_MODEL_PATH) if not exists(DOC2VEC_MODEL_PATH) else None
mkdir(BERT_MODEL_PATH) if not exists(BERT_MODEL_PATH) else None

DATASET = [PATENTS_PATH, CLEAN_PATENTS_PATH, CLEAN_100K_PATENTS_PATH, IPC_TAGS_PATH, IPC_TAGS_100k_PATH, CITATIONS_PATH]
for PATH in DATASET:
    download_from_google_drive(PATH, name=f'{PATH}') if not exists(PATH) else None

# Reports
REPORTS_PATH = f'{ROOT_PATH}/reports'
EVAL_RESULTS_PATH = f'{REPORTS_PATH}/eval_results'
mkdir(REPORTS_PATH) if not exists(REPORTS_PATH) else None
mkdir(EVAL_RESULTS_PATH) if not exists(EVAL_RESULTS_PATH) else None

# Models
try:
    nltk.data.find('tokenizers/punkt')
except LookupError:
    nltk.download('punkt')
BERT_CHOICE = ["bert-base-nli-mean-tokens", "bert-base-nli-stsb-mean-tokens"][1]
GENSIM_SETTINGS = {"vector_size": 500, "min_count": 4, "epochs": 15, "window": 7, 'dm': 1, 'dm_mean': 1,
                   "dm_concat": 0, "dbow_words": 0}
