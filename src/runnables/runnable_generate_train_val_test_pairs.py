from src.data import PatentsPairsDataFrame, PatentsDataset
from src import CITATIONS_PATH, CLEAN_PATENTS_PATH, TAGS_PATH
from src.models.similarities import BidirXCitationSimilarity


if __name__ == "__main__":
    patents = PatentsDataset(CLEAN_PATENTS_PATH,
                             from_year=2000,
                             ipc_tags_format='list',
                             clean_df=False,
                             path_to_citations=CITATIONS_PATH,
                             path_to_ipc_tags_embeddings_pkl=f'{TAGS_PATH}/sf=0.5.pkl')

    # PatentsPairsDataFrame sampling
    # Train
    train_pairs = PatentsPairsDataFrame(patents, name='train', n_samples=1600, n_pos_neg_per_sample=500)
    _ = BidirXCitationSimilarity(patents.citations_graph).evaluate(train_pairs)
    train_pairs.save_pairs_df(columns_to_drop=['ipc_bert_x', 'ipc_bert_y'])

    # Validation
    queries_to_exclude_val = list(train_pairs.pairs_df.pub_number_x.unique()) + list(train_pairs.pairs_df.pub_number_y.unique())
    val_pairs = PatentsPairsDataFrame(patents, name='val', queries_to_exclude=queries_to_exclude_val,
                                      n_samples=50, n_pos_neg_per_sample=500, random_seed=4242)
    _ = BidirXCitationSimilarity(patents.citations_graph).evaluate(val_pairs)
    val_pairs.save_pairs_df(columns_to_drop=['ipc_bert_x', 'ipc_bert_y'])

    # # Test
    queries_to_exclude_test = queries_to_exclude_val + \
        list(val_pairs.pairs_df.pub_number_x.unique()) + list(val_pairs.pairs_df.pub_number_y.unique())
    test_pairs = PatentsPairsDataFrame(patents, name='test', queries_to_exclude=queries_to_exclude_test,
                                       n_samples=800, n_pos_neg_per_sample=500)
    _ = BidirXCitationSimilarity(patents.citations_graph).evaluate(test_pairs)
    test_pairs.save_pairs_df(columns_to_drop=['ipc_bert_x', 'ipc_bert_y'])
