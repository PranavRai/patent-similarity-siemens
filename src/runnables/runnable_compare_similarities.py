from os.path import exists
from os import mkdir
import networkx as nx
import pandas as pd

from src.data import PatentsPairsDataFrame, PatentsDataset
from src import DATA_PATH
from src import CITATIONS_PATH, CLEAN_PATENTS_PATH, TAGS_PATH, REPORTS_PATH
from src.models.metrics import mean_reciprocal_rank_matrix
from src.models.similarities import CosineSimilarity
from src.models.similarities import BasicIPCSimilarity, BidirXCitationSimilarity
from src.models.document_models import Doc2VecModel, BertModel
from src.visualization import visualise_mrr_matrix


if __name__ == "__main__":
    patents = PatentsDataset(CLEAN_PATENTS_PATH,
                             from_year=2000,
                             ipc_tags_format='list',
                             clean_df=False,
                             path_to_citations=CITATIONS_PATH,
                             path_to_ipc_tags_embeddings_pkl=f'{TAGS_PATH}/sf=0.5.pkl')

    test_pairs = PatentsPairsDataFrame(patents=patents,
                                       from_csv=f'{DATA_PATH}/test_pairs_800*500.csv', name='test')

    # Calculating similarities
    doc2vec = Doc2VecModel()
    pretrained_bert = BertModel()
    similarities = [
        BidirXCitationSimilarity(citations_graph=patents.citations_graph),
        BasicIPCSimilarity(levels=5),
        CosineSimilarity(feature='ipc_bert', load_or_calc_embeddings=False),
        CosineSimilarity(model=doc2vec, txt_feature='title', save_calculated_embeddings=True),
        CosineSimilarity(model=doc2vec, txt_feature='abstract', save_calculated_embeddings=True),
        CosineSimilarity(model=pretrained_bert, txt_feature='title', save_calculated_embeddings=True),
        CosineSimilarity(model=pretrained_bert, txt_feature='abstract', save_calculated_embeddings=True),
    ]

    for similarity in similarities:
        similarity.evaluate(test_pairs)

    # Calculating mean reciprocal rank
    sim_names = sorted(test_pairs.similarity_columns)
    mrr_matrix = mean_reciprocal_rank_matrix(test_pairs, sim_names)
    visualise_mrr_matrix(mrr_matrix)
    # mrr_matrix.to_csv(f'{EVAL_RESULTS_PATH}/mrr_{test_pairs.dim[0]}*{test_pairs.dim[1]}.csv', index=True)
