import pandas as pd

from src import CLEAN_PATENTS_PATH, CLEAN_100K_PATENTS_PATH, IPC_TAGS_PATH, IPC_TAGS_100k_PATH
from src.data import PatentsDataset
from src.utils import split_ipc_levels


def generate_ipc_tags_df(dataset: PatentsDataset):
    """
    Generates df with ipc tags, adds extra columns with appended levels from l0-1 to l0-4
    :param dataset: PatentsDataset
    :return: df with ipc tags
    """
    ipc_tags = dataset.patents.apply(lambda patent: pd.Series(patent['ipc']), axis=1).stack().reset_index(level=1, drop=True)
    ipc_tags.name = 'ipc'
    ipc_tags_patents = dataset.patents[['pub_number']].join(ipc_tags)

    ipc_tags_patents['l0'], ipc_tags_patents['l1'], ipc_tags_patents['l2'], ipc_tags_patents['l3'], ipc_tags_patents['l4'] = \
        zip(*ipc_tags_patents.ipc.apply(split_ipc_levels))

    ipc_tags_patents['l0-1'] = ipc_tags_patents['l0'] + ipc_tags_patents['l1']
    ipc_tags_patents['l0-2'] = ipc_tags_patents['l0-1'] + ipc_tags_patents['l2']
    ipc_tags_patents['l0-3'] = ipc_tags_patents['l0-2'] + ipc_tags_patents['l3']
    return ipc_tags_patents


if __name__ == '__main__':
    # Creating many-to-many patents/tags dataframe
    data = PatentsDataset(CLEAN_PATENTS_PATH, ipc_tags_format='list', clean_df=False)
    ipc_tags_patents_df = generate_ipc_tags_df(data)
    ipc_tags_patents_df.to_csv(IPC_TAGS_PATH, index=False)

    data = PatentsDataset(CLEAN_100K_PATENTS_PATH, ipc_tags_format='list', clean_df=False)
    ipc_tags_patents_df = generate_ipc_tags_df(data)
    ipc_tags_patents_df.to_csv(IPC_TAGS_100k_PATH, index=False)
