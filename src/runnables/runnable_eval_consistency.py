from os.path import exists
from os import mkdir
import networkx as nx
import pandas as pd
import logging

from src.data import PatentsPairsDataFrame
from src.data import PatentsDataset
from src import DATA_PATH
from src import CITATIONS_PATH, CLEAN_PATENTS_PATH, TAGS_PATH, EVAL_RESULTS_PATH
from src.models.metrics import mean_reciprocal_rank_matrix
from src.models.similarities import CosineSimilarity
from src.models.similarities import BasicIPCSimilarity, BidirXCitationSimilarity
from src.models.document_models import Doc2VecModel, BertModel


patents = PatentsDataset(CLEAN_PATENTS_PATH,
                         from_year=2000,
                         ipc_tags_format='list',
                         clean_df=False,
                         path_to_citations=CITATIONS_PATH,
                         path_to_ipc_tags_embeddings_pkl=f'{TAGS_PATH}/sf=0.5.pkl')

train_pairs = PatentsPairsDataFrame(from_csv=f'{DATA_PATH}/train_pairs_1600*500.csv', name='train')
val_pairs = PatentsPairsDataFrame(from_csv=f'{DATA_PATH}/val_pairs_50*500.csv', name='val')
queries_to_exclude = list(train_pairs.pairs_df.pub_number_x.unique()) + list(train_pairs.pairs_df.pub_number_y.unique()) + \
    list(val_pairs.pairs_df.pub_number_x.unique()) + list(val_pairs.pairs_df.pub_number_y.unique())

logging.info(f'Number of patents to sample test pairs: {(len(patents.patents) - len(set(queries_to_exclude))):,}')

doc2vec = Doc2VecModel()
pretrained_bert = BertModel()
similarities = [
    BidirXCitationSimilarity(citations_graph=patents.citations_graph),
    BasicIPCSimilarity(levels=5),
    CosineSimilarity(feature='ipc_bert', load_or_calc_embeddings=False),
    CosineSimilarity(model=doc2vec, txt_feature='title', save_calculated_embeddings=False),
    CosineSimilarity(model=doc2vec, txt_feature='abstract', save_calculated_embeddings=False),
    CosineSimilarity(model=pretrained_bert, txt_feature='title', save_calculated_embeddings=False),
    CosineSimilarity(model=pretrained_bert, txt_feature='abstract', save_calculated_embeddings=False),
]

n_samples_list = [500, 1000, 5000, 10000]
n_pos_neg_per_sample_list = [500]

# n_samples_list = [500]
# n_pos_neg_per_sample_list = [500, 1000, 5000, 10000, 50000]

for n_samples in n_samples_list:
    for n_pos_neg_per_sample in n_pos_neg_per_sample_list:
        # Generating test pairs
        test_pairs = PatentsPairsDataFrame(patents, name='test', queries_to_exclude=queries_to_exclude,
                                           n_samples=n_samples, n_pos_neg_per_sample=n_pos_neg_per_sample)

        # Calculating similarities
        for similarity in similarities:
            similarity.evaluate(test_pairs)

        # Calculating mean reciprocal rank
        sim_names = sorted(test_pairs.similarity_columns)
        mrr_matrix = mean_reciprocal_rank_matrix(test_pairs, sim_names)
        mrr_matrix.to_csv(f'{EVAL_RESULTS_PATH}/mrr_{test_pairs.dim[0]}*{test_pairs.dim[1]}.csv', index=True)
