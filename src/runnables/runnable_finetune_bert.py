from torch.utils.data import DataLoader
import logging
import torch
from torch.optim import SGD
from datetime import datetime
from tqdm import tqdm
import numpy as np

from src import DATA_PATH, BERT_MODEL_PATH, BERT_CHOICE
from src.models.document_models import BertModel
from src.models.supervised_losses import TripletLoss
from src.data import PatentsPairsDataFrame
from src.models.similarities import CosineSimilarity
from src.models.metrics import mean_reciprocal_rank

# Read the dataset
train_batch_size = 4
num_epochs = 10
model_save_path = f'{BERT_MODEL_PATH}/training_stsbenchmark_continue_training-' + BERT_CHOICE + '-' + \
                  datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
logging.info(f'Batch size: {train_batch_size}')

# Load a pre-trained sentence transformer model
model = BertModel(freeze_backbone=True, add_fc_layer=True, output_dim=500, fc_layer_act=torch.tanh)
train_loss = TripletLoss(model=model, triplet_margin=1)
optimizer = SGD(train_loss.parameters(), lr=0.0001)
cos_sim_evaluator = CosineSimilarity(model=model, txt_feature='abstract', batch_size=train_batch_size)

# Loading data
train_pairs = PatentsPairsDataFrame(from_csv=f'{DATA_PATH}/train_pairs_1600*500.csv', name='train')
val_pairs = PatentsPairsDataFrame(from_csv=f'{DATA_PATH}/val_pairs_50*500.csv', name='val')
test_pairs = PatentsPairsDataFrame(from_csv=f'{DATA_PATH}/test_pairs_800*500.csv', name='test')

# Offline triplet mining
train_triplets = train_pairs.to_triplets_dataset()
val_triplets = val_pairs.to_triplets_dataset()
logging.info(f'Number of triplets: train = {len(train_triplets)}, val = {len(val_triplets)}.')

train_loader = DataLoader(train_triplets, batch_size=train_batch_size, shuffle=False, num_workers=4)
val_loader = DataLoader(val_triplets, batch_size=train_batch_size, shuffle=False, num_workers=4)

# Loop over epochs
print_every = 1
for epoch in range(num_epochs):

    # Losses for current epoch
    losses = {
        'train': {
            'values': [],
            'batch_sizes': []
        },
        'val': {
            'values': [],
            'batch_sizes': [],
        }
    }

    # Training
    model.train()
    tloader = tqdm(train_loader, total=len(train_loader), desc=f'Epoch {epoch + 1} / {num_epochs}. Training')
    for triple_of_batches in tloader:
        # zero the parameter gradients
        optimizer.zero_grad()
        loss = train_loss(triple_of_batches)
        loss.backward()
        # torch.nn.utils.clip_grad_norm_(train_loss.parameters(), 0.25)
        optimizer.step()

        # print statistics
        losses['train']['values'].append(loss.item())
        losses['train']['batch_sizes'].append(len(triple_of_batches[0]))
        mean_loss = (np.array(losses['train']['values']) *
                     np.array(losses['train']['batch_sizes'])).sum() / sum(losses['train']['batch_sizes'])
        tloader.set_postfix({'Loss': loss.item(), 'Mean Loss': mean_loss})

    # Validation
    model.eval()
    vloader = tqdm(val_loader, total=len(val_loader), desc=f'Epoch {epoch + 1} / {num_epochs}. Validation')
    for triple_of_batches in vloader:
        with torch.no_grad():
            loss = train_loss(triple_of_batches)

        # print statistics
        losses['val']['values'].append(loss.item())
        losses['val']['batch_sizes'].append(len(triple_of_batches[0]))
        mean_loss = (np.array(losses['val']['values']) *
                     np.array(losses['val']['batch_sizes'])).sum() / sum(losses['val']['batch_sizes'])
        vloader.set_postfix({'Loss': loss.item(), 'Mean Loss': mean_loss})

    # MRR on validatrion subset
    cos_sim_evaluator.evaluate(val_pairs)
    logging.info(f'Validation MRR beetween {cos_sim_evaluator.name} and x_citation_1_hop_sim: '
                 f'{mean_reciprocal_rank(val_pairs, cos_sim_evaluator.name, "x_citation_1_hop_sim")}')
