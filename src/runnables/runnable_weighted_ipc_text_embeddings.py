from features import ipc_text_embeddings
import pickle
import os

from src import TAGS_PATH


def predecessor(ipc_tag: str):
    if len(ipc_tag) <= 1:
        return None
    elif len(ipc_tag) == 3:
        return ipc_tag[0]
    elif len(ipc_tag) == 4:
        return ipc_tag[:3]
    elif ipc_tag[ipc_tag.rfind("/") + 1:] == "00":
        return ipc_tag[:4]
    else:
        return ipc_tag[:ipc_tag.rfind("/") + 1] + "00"


def compute_all(scaling_factor: float = 0.5):
    simple_file = TAGS_PATH + "/simple_embeddings.pkl"
    sf_file = TAGS_PATH + f"/sf={scaling_factor}.pkl"
    if os.path.isfile(sf_file):
        return

    if not os.path.isfile(simple_file):
        ipc_text_embeddings.load_all()

    with open(simple_file, "rb") as r:
        embeddings_map = pickle.load(r)
    print("Simple text embeddings retrieved")

    new_embeddings_map = {}
    # lengths: {0, 1 (B), 3 (B68), 4 (B68C), 8 (B68C1/00), 9 (B68C11/00),
    #           10 (B68C171/00), 11 (B68C171/037), 12 (B68C171/0037)}

    # similarities = []
    for key in embeddings_map.keys():
        weight, total_weight = scaling_factor, 1
        parent = predecessor(key)
        new_embedding = embeddings_map[key]
        # print(f"key: {key}")
        while parent:
            total_weight += weight
            new_embedding += embeddings_map[parent] * weight
            # print(f"parent: {parent} \t parent's_weight: {weight} \t total_: {total_weight}")
            parent = predecessor(parent)
            # print(f"new_parent: {parent}\n")
            weight *= scaling_factor
        new_embeddings_map[key] = new_embedding / total_weight
        # similarities.append(cosine(embeddings_map[key], new_embeddings_map[key]))
        # print("--" * 10 + "\n")
    with open(sf_file, "wb") as w:
        pickle.dump(new_embeddings_map, w)

    # import matplotlib.pyplot as plt
    # plt.hist(similarities)
    # plt.show()


if __name__ == "__main__":
    compute_all()
