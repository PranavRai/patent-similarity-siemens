import pandas_profiling
import pandas as pd
import os
import matplotlib.pyplot as plt
from nltk.tokenize import word_tokenize
from string import punctuation as puncs
import gensim
import seaborn as sns

from src import CLEAN_100K_PATENTS_PATH, CLEAN_PATENTS_PATH, REPORTS_PATH, WORD2VEC_MODEL_GOOGLE_PATH, IPC_TAGS_PATH, \
    IPC_TAGS_100k_PATH

sns.set()


def main(sample: bool = True):
    if sample:
        texts_file = CLEAN_100K_PATENTS_PATH
        ref_ipc = IPC_TAGS_100k_PATH
    else:
        texts_file = CLEAN_PATENTS_PATH
        ref_ipc = IPC_TAGS_PATH

    data = pd.read_csv(texts_file)
    print(f"Texts data: {data.columns.values.tolist()}")

    # generate_patents_cleaned_eda(data)

    data = data.drop(["ipc", "date"], axis=1)
    data_ref = pd.read_csv(ref_ipc).drop_duplicates(subset=["pub_number"])[["pub_number", "l0", "l0-1"]]
    data_full = pd.merge(data, data_ref, on=["pub_number"])
    print(f"Merged data {data_full.columns.values.tolist()}")
    del data, data_ref
    word_lengths(data_full, level=0)
    # word_lengths(data_full, level=1)
    oov_word2vec(data_full, level=0)


def word_lengths(data: pd.DataFrame, level: int = 0):
    """
    Computes how many words exist in the given category. We provide histograms as well as overall mean bar charts.
    :param data: Cleaned and title+abstract expanded pandas dataframe
    :param level: Level 0->{'A'-'H'}, and so on for IPC tags
    :return:
    """
    group_index = "l0" if level == 0 else "l0-1"

    uniques = data[group_index].unique()
    groups = data.groupby(group_index)
    for column_name in ["title", "abstract"]:
        save_path = REPORTS_PATH + f"/figures/Length_{column_name}_Level_{level}"
        means, names = [], []
        if os.path.exists(save_path + ".png"):
            continue

        fig, ax = plt.subplots(nrows=len(uniques), ncols=1, sharex=True, sharey=True, figsize=(8, 6))
        for index, (name, group) in enumerate(groups):
            strings = group[column_name].values.tolist()
            strings = [len(word_tokenize(string)) for string in strings]

            names.append(name)
            means.append(sum(strings) / len(strings))

            if column_name == "title":
                x_range = 25
                bins = 25
            else:
                x_range = 400
                bins = 40
            ax[index].set_xlim(0, x_range)
            ax[index].hist(strings, bins, density=False, range=(0, x_range))
            ax[index].set_ylabel(name)

        fig.add_subplot(111, frameon=False)
        plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
        plt.grid(False, which='both')
        plt.xticks([], [])
        plt.yticks([], [])
        plt.ylabel('Patents count', labelpad=65)
        plt.title(f"Number of words in {column_name}, grouped by level = {level+1}")
        plt.gcf().subplots_adjust(left=0.15)
        plt.savefig(save_path + "_hist.png", dpi=300)
        print(f"Word-lengths: {column_name} saved")
        plt.clf()

        positions = list(range(len(names)))
        plt.figure(figsize=(6, 4))
        plt.bar(positions, means, align='center', alpha=0.5)
        plt.xticks(positions, names)
        plt.title(f"Mean word lengths of {column_name}, grouped by level = {level+1}")
        plt.savefig(save_path + ".png", dpi=300)
        plt.clf()


def oov_word2vec(data: pd.DataFrame, level: int = 0):
    """
    Computes how many words can't be found in Word2Vec's vocabulary. Both original and lowercase wordforms are tested,
    punctuations are ignored. We provide histograms as well as overall mean bar charts.
    :param data: Cleaned and title+abstract expanded pandas dataframe
    :param level: Level 0->{'A'-'H'}, and so on for IPC tags
    :return:
    """
    group_index = "l0" if level == 0 else "l0-1"

    uniques = data[group_index].unique()
    groups = data.groupby(group_index)
    wv_vocab = gensim.models.KeyedVectors.load_word2vec_format(WORD2VEC_MODEL_GOOGLE_PATH, binary=True).vocab
    for column_name in ["title", "abstract"]:
        save_path = REPORTS_PATH + f"/figures/OOV_{column_name}_Level_{level}"
        means, names = [], []
        if os.path.exists(save_path + ".png"):
            continue

        fig, ax = plt.subplots(nrows=len(uniques), ncols=1, sharex=True, sharey=True, figsize=(8, 6))
        for index, (name, group) in enumerate(groups):
            strings = group[column_name].values.tolist()
            strings = [word_tokenize(string) for string in strings]
            strings = [[word for word in string if word not in puncs] for string in strings]
            oov_counts = [(1 - len([word for word in string if word in wv_vocab or word.lower() in wv_vocab
                                    ]) * 1.0 / len(string)) for string in strings]
            names.append(name)
            means.append(sum(oov_counts) / len(oov_counts))
            ax[index].set_xlim(0.0, 0.5)
            ax[index].hist(oov_counts, 60, range=(0.0, 1.0), density=False)
            ax[index].set_ylabel(name)

        fig.add_subplot(111, frameon=False)
        plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
        plt.grid(False, which='both')
        plt.xticks([], [])
        plt.yticks([], [])
        plt.ylabel('Patents count', labelpad=65)
        plt.xlabel('Percentage of OOV words in patent', labelpad=30)
        plt.title(f"OOV in Word2Vec (Google news corpus) for {column_name}, grouped by level = {level+1}")
        plt.gcf().subplots_adjust(left=0.15)
        plt.savefig(save_path + "_hist.png", dpi=300)
        plt.clf()

        positions = list(range(len(names)))
        plt.figure(figsize=(6, 4))
        plt.bar(positions, means, align='center', alpha=0.5)
        plt.xticks(positions, names)
        plt.ylabel('Mean percentage of OOV words in patent')
        plt.title(f"Mean OOV in Word2Vec for {column_name}, grouped by level = {level+1}")
        plt.gcf().subplots_adjust(left=0.2)
        plt.savefig(save_path + ".png", dpi=300)
        plt.clf()


def generate_patents_cleaned_eda(data: pd.DataFrame):
    """
    Generates pandas profile report for the cleaned version of data
    :param data: Cleaned pandas dataframe
    :return:
    """
    filename = f'{REPORTS_PATH}/eda_sample_clean.html'
    if not os.path.isfile(filename):
        profile = data.profile_report()
        profile.to_file(output_file=filename)


if __name__ == "__main__":
    main()
