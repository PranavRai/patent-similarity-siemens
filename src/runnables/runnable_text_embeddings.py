import argparse
import logging

from src.data import PatentsDataset
from src.utils import md5_hash
from src import CLEAN_100K_PATENTS_PATH, CLEAN_PATENTS_PATH
from src.models.document_models import Doc2VecModel, BertModel


if __name__ == "__main__":
    embeddings = []

    parser = argparse.ArgumentParser()
    parser.add_argument("--full_sample", help="Are we working on sample of the data? True/False",
                        action="store_true", default=False)
    parser.add_argument("--embedding", help="doc2vec / bert", default="doc2vec")
    args = parser.parse_args()

    # Preparing patents
    texts_file = CLEAN_100K_PATENTS_PATH if not args.full_sample else CLEAN_PATENTS_PATH
    patents = PatentsDataset(texts_file, ipc_tags_format='list', clean_df=False)
    patents.patents = patents.patents.drop(["ipc", "date"], axis=1)
    pub_numbers_hash = md5_hash(patents.patents.pub_number)
    logging.info(f"Sample: {args.full_sample}, embedding choice: {args.embedding}, "
                 f"hash of pub_numbers in dataset: {pub_numbers_hash}")

    # Loading models
    if args.embedding == "doc2vec":
        model = Doc2VecModel()
    elif args.embedding == "bert":
        model = BertModel()
    else:
        raise Exception('Non-existing embedding choice')

    for txt_feature in ["abstract", "title"]:
        embeddings = model.get_embeddings(patents.patents[txt_feature].values)
        embeddings_map = {k: v for (k, v) in zip(patents.patents.pub_number, embeddings)}
        model.save_embeddings(embeddings_map=embeddings_map, txt_feature=txt_feature, pub_numbers_hash=pub_numbers_hash)
