from src import PATENTS_PATH, CLEAN_PATENTS_PATH, CLEAN_100K_PATENTS_PATH
from src.data import PatentsDataset

if __name__ == '__main__':
    # Saving cleaned data
    data = PatentsDataset(PATENTS_PATH, ipc_tags_format='semicolon', clean_df=True)
    data.patents.to_csv(CLEAN_PATENTS_PATH, index=False)

    # Saving 100k sample cleaned data, after 2000
    sample_size = 100000
    data = PatentsDataset(CLEAN_PATENTS_PATH, from_year=2000, ipc_tags_format='list', clean_df=True)
    data.patents.sample(sample_size, random_state=42).to_csv(CLEAN_100K_PATENTS_PATH, index=False)
