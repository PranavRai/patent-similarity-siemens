import torch
from pytorch_pretrained_bert import BertTokenizer, BertModel
import logging


def get_embeddings(documents: list):
    documents, segmented_docs = preprocess_sentence(documents)
    print(documents)
    print(segmented_docs)

    tokens_tensor = torch.tensor(documents)
    segments_tensors = torch.tensor(segmented_docs)

    model = BertModel.from_pretrained('bert-base-uncased')
    model.eval()
    with torch.no_grad():
        encoded_layers, _ = model(tokens_tensor, segments_tensors)
    print("Number of layers:", len(encoded_layers))
    layer_i = 0

    print("Number of batches:", len(encoded_layers[layer_i]))
    batch_i = 0

    print("Number of tokens:", len(encoded_layers[layer_i][batch_i]))
    token_i = 0

    print("Number of hidden units:", len(encoded_layers[layer_i][batch_i][token_i]))

    token_embeddings = torch.stack(encoded_layers, dim=0)
    print(encoded_layers)
    print(token_embeddings.size())
    # token_embeddings = torch.squeeze(token_embeddings, dim=1)
    # token_embeddings.size()

    return encoded_layers[-1]


def preprocess_sentence(documents: list):
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    marked_docs = [tokenizer.tokenize("[CLS] " + document + " [SEP]") for document in documents]
    marked_docs = [tokenizer.convert_tokens_to_ids(marked_doc) for marked_doc in marked_docs]
    # use nltk.sent_tokenize to mark sentences
    segmented_docs = [[index + 1] * len(marked_doc) for index, marked_doc in enumerate(marked_docs)]
    # marked_docs = [val for sublist in marked_docs for val in sublist]
    # segmented_docs = [val for sublist in segmented_docs for val in sublist]
    return marked_docs, segmented_docs


if __name__ == "__main__":
    toy_documents = ["Here is the sentence I want embeddings for."
                     # "After breaking the text into tokens, we then have to convert the sentence from a list of
                     # strings to a list of vocabulary indeces."
                     # "This should be suitable for many users. Anaconda is our recommended package manager since it
                     # installs all dependencies."
                     ]
    get_embeddings(toy_documents)
