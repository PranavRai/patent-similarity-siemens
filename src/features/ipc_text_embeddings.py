
import urllib.request
import nltk
import numpy as np
import os
import pickle

from src import IPC_URL, TAGS_PATH
from src.models.document_models import BertModel
# Note:
# 1) Anomaly/frequent items
# A99Z	SUBJECT MATTER NOT OTHERWISE PROVIDED FOR IN THIS SECTION
# A99Z0099000000	Subject matter not otherwise provided for in this section
# 2) Some sentences were single word / too small
# 3) replacing long within-text tags with <unk>: A01D0043120000;soil working for engineering purposes E01, E02, E21
# Ideas
# 1) Splitting long sentences further at semi colons?


def load_all():
    simple_file = TAGS_PATH + "/simple_embeddings.pkl"
    if os.path.isfile(simple_file):
        return

    tags, embeddings = [], []
    for header in ["A", "B", "C", "D", "E", "F", "G", "H"]:
        url = IPC_URL.replace("_A_", "_" + header + "_")
        save_path = TAGS_PATH + "/" + header + ".txt"
        urllib.request.urlretrieve(url, save_path)
        with open(save_path, "r", encoding="utf-8") as r:
            text = r.read().strip()

        sentences = text.split("\n")
        positions = [sentence.find("\t") for sentence in sentences]
        positions = [sentences[index].find(" ") if positions[index] < 0 else positions[index]
                     for index in range(len(sentences))]
        tags = tags + [sentences[index][:positions[index]] for index in range(len(sentences))]
        sentences = [sentences[index][positions[index] + 1:] for index in range(len(sentences))]
        sentences = preprocess(sentences)

        model = BertModel(level='sentence')
        if header == "A":
            embeddings = np.array(model.get_embeddings(sentences, batch_size=64))
        else:
            embeddings = np.vstack((embeddings, np.array(model.get_embeddings(sentences, batch_size=64))))

    # A01B0001000000 -> A01B1/00
    tags = [tag[:4] + tag[4:8][::-1].rstrip("0")[::-1] + "/" + tag[8:10] + tag[10:].rstrip("0") for tag in tags]
    tags = [tag.rstrip("/") for tag in tags]

    embeddings_map = {tag: embedding for tag, embedding in zip(tags, embeddings)}
    # print(tags[:100])
    # print(embeddings_map["A01"].shape)
    with open(simple_file, "wb") as w:
        pickle.dump(embeddings_map, w)


def preprocess(sentences: list):
    # sentences = [sentence.replace(";", "; ") for sentence in sentences]
    detokenizer = nltk.tokenize.treebank.TreebankWordDetokenizer()

    def detect_replace_tags(sentence):
        sentence = nltk.word_tokenize(sentence)
        sentence = ["[UNK]" if sum(c.isdigit() for c in word) / len(word) > 0.1 and len(word) > 4 else word
                    for word in sentence]
        sentence = detokenizer.detokenize(sentence)
        return sentence
    sentences = [detect_replace_tags(sentence) for sentence in sentences]
    return sentences
