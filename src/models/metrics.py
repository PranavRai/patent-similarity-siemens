import numpy as np
import pandas as pd
from typing import List

from src.data import PatentsPairsDataFrame


def mean_reciprocal_rank(pairs_df: PatentsPairsDataFrame, proposed_sim: str, ground_truth_sim: str,
                         query_col='pub_number_x') -> float:
    """
    Calculates MRR
    :param pairs_df: PatentsPairsDataFrame for evaluation
    :param proposed_sim: Name of proposed similarity
    :param ground_truth_sim: Name of ground truth similarity
    :param query_col: Column with pub_numbers of queries (anchors)
    :return: MRR value
    """
    cross_df = pairs_df.pairs_df

    # Dropping self-similarities
    cross_df = cross_df[cross_df.pub_number_x != cross_df.pub_number_y]

    gt_sorting = cross_df.sort_values(by=ground_truth_sim, ascending=False)
    # Ties in proposed_sim -> average
    gt_sorting['proposed_ranks'] = gt_sorting.groupby(query_col)[proposed_sim].rank(ascending=False,
                                                                                    method='average',
                                                                                    na_option='bottom')

    gt_sorting['gt_ranks'] = gt_sorting.groupby(query_col)[ground_truth_sim].rank(ascending=False,
                                                                                  method='min',
                                                                                  na_option='bottom')
    # Ties in ground-truth -> average
    return np.mean(1 / gt_sorting[gt_sorting.gt_ranks == 1.0].groupby(query_col)['proposed_ranks'].mean())


def mean_reciprocal_rank_matrix(pairs_df: PatentsPairsDataFrame, sim_names: List[str]):
    mrr_matrix = pd.DataFrame(data=[[mean_reciprocal_rank(pairs_df, ps, gts) for ps in sim_names] for gts in sim_names],
                              columns=sim_names,
                              index=sim_names)
    return mrr_matrix


def hits_at():
    pass
