from sentence_transformers.losses import TripletDistanceMetric
from torch import nn
import torch.nn.functional as F
from typing import Collection, Tuple

from src.models.document_models import BertModel


class TripletLoss(nn.Module):
    """Supervised loss for metric learning"""
    def __init__(self, model: BertModel, distance_metric=TripletDistanceMetric.EUCLIDEAN, triplet_margin=1):
        super(TripletLoss, self).__init__()
        self.model = model
        self.distance_metric = distance_metric
        self.triplet_margin = triplet_margin

    def forward(self, triple_of_document_batches: Tuple[Collection[str], Collection[str], Collection[str]]):
        assert len(triple_of_document_batches) == 3
        reps = [self.model(documents_batch) for documents_batch in triple_of_document_batches]

        rep_anchor, rep_pos, rep_neg = reps
        distance_pos = self.distance_metric(rep_anchor, rep_pos)
        distance_neg = self.distance_metric(rep_anchor, rep_neg)

        losses = F.relu(distance_pos - distance_neg + self.triplet_margin)
        return losses.mean()
