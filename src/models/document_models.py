from abc import ABC, abstractmethod

import nltk
from torch import nn
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from sentence_transformers import SentenceTransformer
from sentence_transformers.models import Pooling
from typing import List, Mapping, Dict
import torch
import torch.nn.functional as F
import numpy as np
import pickle
import os
import logging
from tqdm import tqdm

from src import BERT_CHOICE, DOC2VEC_MODEL_PATH, GENSIM_SETTINGS, TEXT_EMBEDDING_PATH


class DocumentModel(ABC):
    """Abstract class for embedding models for documents"""

    @abstractmethod
    def get_embeddings(self, documents: List[str], **kwargs) -> List[np.array]:
        """
        Embed list of documents
        :param documents: List of raw documents
        :param kwargs: Model specific arguments (e.g. batch_size)
        :return: List of embeddings, as list of np.arrays
        """
        ...

    @abstractmethod
    def get_embedding(self, document: str) -> np.array:
        """
        Embed single document
        :param document: Raw document
        :return: Embedding, np.array
        """
        ...

    @abstractmethod
    def get_unified_path_for_embeddings(self, **kwargs) -> str:
        """
        Get path to store/load embeddings map pickle
        :param kwargs: Model specific arguments
        :return: Path to pickle
        """
        ...

    def load_embeddings(self, **kwargs) -> Dict[str, np.array] or None:
        """
        Loads existing embeddings map from resources
        :return: embeddings map (e.g. pub_number: embedding)
        """
        embeddings_path = self.get_unified_path_for_embeddings(**kwargs)
        if os.path.isfile(embeddings_path):
            logging.info(f"Retrieving existing embeddings...")
            with open(embeddings_path, "rb") as r:
                return pickle.load(r)
        else:
            logging.info(f"No existing embeddings found.")
            return None

    def save_embeddings(self, embeddings_map: Mapping[str, np.array], **kwargs):
        """
        Saves embeddings to resources
        :param embeddings_map: Embeddings map to dump as pickle (e.g. pub_number: embedding)
        """
        embeddings_path = self.get_unified_path_for_embeddings(**kwargs)
        with open(embeddings_path, "wb") as w:
            pickle.dump(embeddings_map, w)
        logging.info(f"Embeddings saved as {embeddings_path}.")


class Doc2VecModel(DocumentModel):
    """Wrapper for gensim.models.doc2vec.Doc2Vec, inheriting DocumentModel"""
    def __init__(self, gensim_settings: dict = GENSIM_SETTINGS):
        """
        :param gensim_settings: Configuration for Gensim's doc2vec
        """
        self.name = 'doc2vec'
        self.gensim_settings = gensim_settings
        self.gensim_config = "_vs=" + str(self.gensim_settings["vector_size"]) + "_e=" + str(self.gensim_settings["epochs"])
        # + "_mc=" + str(gensim_settings["min_count"]) + "_w=" + str(gensim_settings["window"]) + "_dm=" + str(
        #         gensim_settings['dm']) + str(gensim_settings['dm_mean']) + str(gensim_settings['dm_concat'])
        #         + "_dbow=" + str(gensim_settings["dbow_words"])
        self.model_path = f"{DOC2VEC_MODEL_PATH}/Doc2Vec{self.gensim_config}"

        if os.path.isfile(self.model_path):
            logging.info("Retrieving saved Doc2Vec model...")
            self.model = Doc2Vec.load(self.model_path)
        else:
            logging.info("Doc2Vec model not found - need to build one!")
            self.model = None

    @staticmethod
    def _tokenize_documents(documents: List[str], lower: bool = True) -> List[List[str]]:
        """
        Private method for appropriate preprocessing for the documents (abstracts/titles)
        :param documents: [doc_1, ... doc_n] where doc_i is a string representing the document
        :param lower: True-> lowercase the document texts
        :return: list of documents, each preprocessed
        """
        if lower:
            documents = [nltk.sent_tokenize(doc.lower()) for doc in documents]
        else:
            documents = [nltk.sent_tokenize(doc) for doc in documents]
        documents = [[nltk.word_tokenize(sentence) for sentence in doc] for doc in documents]
        documents = [[word for sentence in doc for word in sentence] for doc in documents]
        return documents

    def get_embedding(self, document: str) -> np.array:
        document = self._tokenize_documents([document])[0]
        document = np.array([self.model.infer_vector(document)])
        return document

    def get_embeddings(self, documents: List[str], **kwargs) -> List[np.array]:
        documents = self._tokenize_documents(documents)
        documents = [self.model.infer_vector(doc) for doc in tqdm(documents)]
        return documents

    def get_unified_path_for_embeddings(self, txt_feature, pub_numbers_hash) -> str:
        """
        Get path to store/load embeddings map pickle
        :param txt_feature: "abstract"/"title"
        :param pub_numbers_hash: MD5 hash of publishing numbers
        :return: Path to pickle
        """
        unified_path = TEXT_EMBEDDING_PATH + "/" + txt_feature + "_doc2vec" + "_vs=" + str(self.gensim_settings["vector_size"])
        unified_path += "_e=" + str(self.gensim_settings["epochs"]) + '_hash=' + pub_numbers_hash + '.pkl'
        return unified_path

    def train(self, documents: list) -> Doc2Vec:
        """
        Builds the doc2vec model
        :param documents: [doc_1, ... doc_n] where doc_i is a list of word tokens for that doc. Don't remove stops/puncs!
        :return: built model object
        """
        logging.info("Building Doc2Vec model...")
        self.model = Doc2Vec(**self.gensim_settings)
        documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(documents)]

        self.model.build_vocab(documents)
        self.model.train(documents, total_examples=self.model.corpus_count, epochs=self.model.epochs)
        logging.info("Doc2Vec model built!")

        self.model.save(self.model_path)
        logging.info("Model saved")

        return self.model


class BertModel(nn.Module, DocumentModel):
    """
    Wrapper for sentence_transformers.SentenceTransformer, which extends it to a document level model.
    Class also has a possibility to add trainable fc layer, freeze bert backbone or fine tune model
    """
    def __init__(self, level='document', transformer_choice: str = BERT_CHOICE, add_fc_layer=False, fc_layer_act=None,
                 output_dim=768, freeze_backbone=False):
        """
        :param level: Level of model (document, sentence). If 'sentence' is chosen, act like simple
            sentence_transformers.SentenceTransformer
        :param transformer_choice: Choice of transformer (name or path). See model_name_or_path parameter of SentenceTransformer
        :param add_fc_layer: Add FC connected layer after document level pooling
        :param output_dim: Output dimensionality for FC connected layer
        :param freeze_backbone: Trainability of bert backbone
        """
        super().__init__()
        self.name = 'bert' if not add_fc_layer else 'bert+fc'
        self.level = level
        self.sentence_transformer = SentenceTransformer(transformer_choice)
        if freeze_backbone:
            for param in self.sentence_transformer.parameters():
                param.requires_grad = False
        self.document_mean_pooling = Pooling(self.sentence_transformer.get_sentence_embedding_dimension())
        self.output_dim = self.sentence_transformer.get_sentence_embedding_dimension()
        if add_fc_layer:
            self.fc = nn.Linear(self.sentence_transformer.get_sentence_embedding_dimension(), output_dim)
            self.act = fc_layer_act
            self.output_dim = output_dim
        self.to(self.sentence_transformer.device)

    def sentences_to_features(self, sentences: List[str]) -> Dict:
        """
        Transforms list of sentences to dict of torch.tensors with padded sequences and sentence masks
        :param sentences: List of sentences
        :return: dict of torch.tensors
        """
        # Copy-paste from encode method of SentenceTransformer
        longest_seq = 0
        batch_tokens = []

        for sentence in sentences:
            tokens = self.sentence_transformer.tokenize(sentence)
            longest_seq = max(longest_seq, len(tokens))
            batch_tokens.append(tokens)

        features = {}
        for token in batch_tokens:
            sentence_features = self.sentence_transformer.get_sentence_features(token, longest_seq)

            for feature_name in sentence_features:
                if feature_name not in features:
                    features[feature_name] = []
                features[feature_name].append(sentence_features[feature_name])

        for feature_name in features:
            features[feature_name] = torch.tensor(np.asarray(features[feature_name])).to(self.sentence_transformer.device)

        return features

    def documents_to_features(self, sentences_embeddings: dict, document_lengths: List[int]) -> Dict:
        """
        Transforms dict of torch.tensors with senteces_embeddings to dict of torch.tensors suitable for document-level pooling
        :param sentences_embeddings: dict of torch.tensors with senteces_embeddings
        :param document_lengths: list of documents lenghts, used for senteces_embeddings regrouping and pooling
        :return: dict of torch.tensors
        """
        features = {}
        document_lengths = np.array(document_lengths)

        features['document_lengths'] = torch.tensor(document_lengths).to(self.sentence_transformer.device)

        # Creating input mask of sentences
        longest_document = max(document_lengths)
        input_mask = np.array([[1] * length + [0] * (longest_document - length) for length in document_lengths])
        features['input_mask'] = torch.tensor(input_mask).to(self.sentence_transformer.device)

        # Reshaping sentence embeddings
        embeddings = torch.empty((len(document_lengths), longest_document, self.output_dim))
        sent_ind = 0
        for ind in range(len(document_lengths)):
            embeddings[ind, :document_lengths[ind]] = \
                sentences_embeddings['sentence_embedding'][sent_ind:sent_ind + document_lengths[ind]]
            sent_ind += document_lengths[ind]
        features['token_embeddings'] = embeddings.to(self.sentence_transformer.device)

        features['cls_token_embeddings'] = None  # Compatibility

        return features

    @staticmethod
    def _tokenize_documents(documents: List[str]) -> (List[List[str]], List[str], List[int]):
        """
        Private method for appropriate preprocessing for the documents (abstracts/titles)
        Transforms list [doc1, doc2, ...] into:
            1. list of lists - [[sent1_doc1, sent2_doc1, ...], [sent1_doc2, sent2_doc2, ...], ...]
            2. flattened version of 1 - [sent1_doc1, sent2_doc1, ..., sent1_doc2, sent2_doc2, ..., ...]
            3. list of numbers of sentences - [size1, size2, ...]
        :param documents: list of sentences/strings
        :return: Triple of documents, sentences, document_lengths
        """
        documents = [nltk.sent_tokenize(document) for document in documents]
        # All sentences across documents flattened to one list for faster computation.
        sentences = [sentence for document in documents for sentence in document]
        document_lengths = [len(document) for document in documents]
        return documents, sentences, document_lengths

    def forward(self, documents_batch: List[str]) -> torch.tensor:
        output = None

        if self.level == 'document':
            documents_batch, sentences, document_lengths = self._tokenize_documents(documents_batch)

            # BERT + Sentences pooling = Backbone
            senteces_features = self.sentences_to_features(sentences)
            senteces_embeddings = self.sentence_transformer(senteces_features)

            if hasattr(self, 'fc'):
                senteces_embeddings['sentence_embedding'] = self.fc(senteces_embeddings['sentence_embedding'])
                if self.act is not None:
                    senteces_embeddings['sentence_embedding'] = self.act(senteces_embeddings['sentence_embedding'])

            # Documents pooling
            documents_features = self.documents_to_features(senteces_embeddings, document_lengths)
            documents_embeddings = self.document_mean_pooling(documents_features)

            output = documents_embeddings['sentence_embedding']

        elif self.level == 'sentence':  # documents_batch is a sentence batch
            # BERT + Sentences pooling = Backbone
            senteces_features = self.sentences_to_features(documents_batch)
            senteces_embeddings = self.sentence_transformer(senteces_features)

            output = senteces_embeddings['sentence_embedding']

        if torch.isnan(output).any():
            logging.error(f'NAN in the output: {output.data}')

        return output

    def get_embedding(self, document: str) -> np.array:
        documents_batch = [document]
        with torch.no_grad():
            embedding = self(documents_batch).cpu().numpy()[0]
        return embedding

    def get_embeddings(self, documents: List[str], batch_size: int = 8, **kwargs) -> List[np.array]:
        """
        Embed list of documents
        :param documents: List of raw documents
        :param batch_size: Batch size
        :return: List of embeddings, as list of np.arrays
        """
        all_embeddings = []
        iterator = tqdm(range(0, len(documents), batch_size))
        for batch_idx in iterator:
            batch_start = batch_idx
            batch_end = min(batch_start + batch_size, len(documents))
            documents_batch = documents[batch_start:batch_end]
            with torch.no_grad():
                embedding = self(documents_batch).cpu().numpy()
            all_embeddings.extend(embedding)
        return all_embeddings

    def get_unified_path_for_embeddings(self, txt_feature, pub_numbers_hash) -> str:
        """
        Get path to store/load embeddings map pickle
        :param txt_feature: "abstract"/"title"
        :param pub_numbers_hash: MD5 hash of publishing numbers
        :return: Path to pickle
        """
        return TEXT_EMBEDDING_PATH + "/" + txt_feature + "_" + self.name + '_hash=' + pub_numbers_hash + '.pkl'
