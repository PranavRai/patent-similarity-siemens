from scipy.spatial.distance import cosine
import networkx as nx
import pandas as pd
import numpy as np
from networkx.algorithms.simple_paths import all_simple_paths
import logging
from abc import ABC, abstractmethod
from typing import List
from tqdm import tqdm

from src.utils import split_ipc_levels, md5_hash
from src.data.datasets import PatentsPairsDataFrame
from src.models.document_models import DocumentModel
tqdm.pandas()


class PatentSimilarity(ABC):
    """Abstract class for similarities calculations"""
    def __init__(self):
        self.name = None
        self.feature = None

    def evaluate(self, pairs: PatentsPairsDataFrame) -> pd.Series:
        """
        Method for calculating similarities for whole pairs_df, joins calculated similarities to pairs_df. Assumes,
        pairs_df have two groups of columns, with suffixes x and y.
        :param pairs: pairs_df
        :return pd.Series with similarities
        """
        logging.info(f'Calculating {self.name}.')
        pairs.pairs_df[self.name] = \
            pairs.pairs_df.progress_apply(
                lambda row: self.calculate_pairwise(row[f'{self.feature}_x'], row[f'{self.feature}_y']), axis=1)
        pairs.similarity_columns.add(self.name)
        return pairs.pairs_df[self.name]

    @abstractmethod
    def calculate_pairwise(self, patent_feature1, patent_feature2) -> float:
        """
        Method for calculating similarities for one pair of patent features
        :param patent_feature1: Features of query (anchor) patent
        :param patent_feature2: Features of negative/positive patent
        :return: Value of similarity
        """
        ...


class BasicIPCSimilarity(PatentSimilarity):
    """
    Calculates basic IPC similarity, based on number of shared levels
    """
    def __init__(self, levels: int = 5):
        """
        :param levels: Number of levels to consider (1-5)
        """
        self.levels = levels
        self.name = f'basic_ipc_sim{self.levels}'
        self.feature = 'ipc'  # List of tags of patent

    def basic_ipc_pairwise_sim(self, tag1: str, tag2: str) -> float:
        """
        Similarity between two tags
        :param tag1: Tag1
        :param tag2: Tag2
        :return: Value of similarity
        """
        tag1_levels = split_ipc_levels(tag1)[:self.levels]
        tag2_levels = split_ipc_levels(tag2)[:self.levels]
        num_of_all_levels = len(tag1_levels)
        num_of_shared_levels = 0
        for l1, l2 in zip(tag1_levels, tag2_levels):
            if l1 == l2:
                num_of_shared_levels += 1
            else:
                break
        return num_of_shared_levels / num_of_all_levels

    def calculate_pairwise(self, tag_list_1: List[str], tag_list_2: List[str]) -> float:
        pairwise_sim = np.array(
            [[self.basic_ipc_pairwise_sim(tag1, tag2) for tag1 in tag_list_1] for tag2 in tag_list_2])
        return np.append(pairwise_sim.max(axis=0).flatten(), pairwise_sim.max(axis=1).flatten()).mean()


class BidirXCitationSimilarity(PatentSimilarity):
    """
    Calculates x-citation similarity, based on number of hops in bidirectional citation graph
    """
    def __init__(self, citations_graph: nx.Graph, n_hops: int = 1, weight: str = 'inverse_exp'):
        """
        :param citations_graph: Citation graph
        :param n_hops: Number of hops
        :param weight: Weighting of path length (inverse_exp, inverse)
        """
        self.citations_graph = citations_graph
        self.n_hops = n_hops
        self.weight = weight
        self.name = f'x_citation_{self.n_hops}_hop_sim'
        self.feature = 'pub_number'

    def calculate_pairwise(self, pub_number1, pub_number2) -> float:
        if pub_number1 == pub_number2:
            return 1.0
        simple_paths = [path for path in
                        all_simple_paths(self.citations_graph, pub_number1, pub_number2, cutoff=self.n_hops + 1)]
        min_path_length = min(list(map(len, simple_paths)), default=None)
        similarity = 0.0
        if self.weight == 'inverse_exp':
            similarity = 1 / 2 ** (min_path_length - 2) if min_path_length is not None else np.nan
        elif self.weight == 'inverse':
            similarity = 1 / (min_path_length - 1) if min_path_length is not None else np.nan
        return similarity


class CosineSimilarity(PatentSimilarity):
    """
    Calculates cosine similarity for textual features (documents/sentences), also calculates embeddings alongways
    """
    def __init__(self, model: DocumentModel = None, txt_feature: str = None, load_or_calc_embeddings=True,
                 save_calculated_embeddings=False, feature: str = None, batch_size=8):
        """
        :param model: DocumentModel for calculating embeddings
        :param txt_feature: Name of textual feature (abstract, title)
        :param load_or_calc_embeddings:
            True - evaluate will search for saved embeddings in RESOURSES and load them (or calculate and save)
            False - assuming that column with embeddings is already present in pairs_df (then need to
            specify it under :param feature)
        :param save_calculated_embeddings: save calculated embeddings, if calculate_embeddings == True and no embeddings found
        :param feature: name of column with embeddings, if :param calculate_embeddings == False
        :param batch_size: Batch size for embeddings calculation
        """
        self.load_or_calc_embeddings = load_or_calc_embeddings
        if self.load_or_calc_embeddings:
            assert model is not None and txt_feature is not None
            self.model = model
            self.txt_feature = txt_feature
            self.feature = f'{self.txt_feature}_{self.model.name}'
            self.save_calculated_embeddings = save_calculated_embeddings
            self.batch_size = batch_size
        else:
            assert feature is not None
            self.feature = feature
        self.name = f'{self.feature}_cosine_sim'

    def _load_or_calc_embeddings(self, pairs: PatentsPairsDataFrame):
        """
        Private method for loading or calculating embeddings. Joins calculated embeddings to pairs_df
        :param pairs: pairs_df
        """

        logging.info(f'Embedding documents {self.name}.')
        for suffix in ['x', 'y']:
            if f'{self.feature}_{suffix}' in pairs.pairs_df.columns:
                pairs.pairs_df.drop(columns=[f'{self.feature}_{suffix}'], inplace=True)
            uniques = pairs.pairs_df.drop_duplicates(subset=f'pub_number_{suffix}')
            pub_numbers_hash = md5_hash(uniques[f'pub_number_{suffix}'])

            # Trying to load embeddings
            embeddings_map = self.model.load_embeddings(txt_feature=self.txt_feature, pub_numbers_hash=pub_numbers_hash)

            if embeddings_map is not None:
                embeddings_series = pd.Series(embeddings_map, name=f'{self.feature}_{suffix}')
            else:  # Calculating embeddings
                documents = uniques[f'{self.txt_feature}_{suffix}'].values
                embeddings = self.model.get_embeddings(documents, batch_size=self.batch_size)
                embeddings_series = pd.Series(embeddings, index=uniques[f'pub_number_{suffix}'],
                                              name=f'{self.feature}_{suffix}')
                if self.save_calculated_embeddings:
                    self.model.save_embeddings(embeddings_map=embeddings_series.to_dict(), txt_feature=self.txt_feature,
                                               pub_numbers_hash=pub_numbers_hash)

            # Joining embeddings series to pairs_df
            pairs.pairs_df = pairs.pairs_df.join(embeddings_series, on=f'pub_number_{suffix}')

    def evaluate(self, pairs: PatentsPairsDataFrame) -> pd.Series:
        if self.load_or_calc_embeddings:
            self._load_or_calc_embeddings(pairs)

        logging.info(f'Calculating {self.name}.')

        values = pairs.pairs_df[[f'{self.feature}_x', f'{self.feature}_y']]
        if values.isna().values.any():  # Some embeddings are missing
            pairs.pairs_df[self.name] = \
                pairs.pairs_df.progress_apply(
                    lambda row: self.calculate_pairwise(row[f'{self.feature}_x'], row[f'{self.feature}_y']), axis=1)
        else:  # All embeddings are present, can put everything to one array
            values = np.array(values.values.tolist())
            pairs.pairs_df[self.name] = self.calculate_pairwise(values[:, 0, :], values[:, 1, :])

        pairs.similarity_columns.add(self.name)
        return pairs.pairs_df[self.name]

    def calculate_pairwise(self, embed1: np.array, embed2: np.array) -> float:
        if len(embed1.shape) <= 1:
            return 1 - cosine(embed1, embed2)
        else:
            if np.isnan(embed1).any() or np.isnan(embed2).any():
                logging.warning('Embeddings contain nans!')
            return np.nansum(embed1 * embed2, axis=1) / (np.sqrt(
                np.nansum(embed1 * embed1, axis=1)) * np.sqrt(np.nansum(embed2 * embed2, axis=1)))
